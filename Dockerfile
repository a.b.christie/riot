ARG from_image=arm32v7/python:3.9.5-slim-buster
FROM ${from_image}

# Force the binary layer of the stdout and stderr streams
# to be unbuffered
ENV PYTHONUNBUFFERED 1

# Install AWS IoT and any other requirments for the base image...
COPY requirements.txt /tmp/

# The awsiotsdk requires a compiler so we need: -
#   cmake, build-essential
#
# Also, install and set timezone info (UTC).
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        cmake \
        build-essential \
        libssl-dev && \
    pip install -r /tmp/requirements.txt
