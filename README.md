# R.IoT
A base image for Raspberry Pi IoT container-based development based on
the *slim* Debian Buster distribution, Python 3.9 and the [AWS IoT SDK].

Images that are used as base images for our application development.

>   If a binary distribution of the AWS CRT (Common Runtime) is not available
    for the build architecture then the CRT is compiled from source,
    which will typically take around 10-20 minutes on GitLab's shared runners.

Currently, we've disbaled the GitLab-CI build - it takes too long -
now over an hour. Building locally (see below), on a MacBook Pro
(2.7 GHz Quad-Core Intel Core i7) will take 25 to 30 minutes.

## Build instructions
Refer to the CI/CD process (the project's `.gitlab-ci.yml`) but
from a suitable host you might be able to build an ARM container image
by just running docker (it works on OSX 11.3.1): -

    $ docker build -t registry.gitlab.com/a.b.christie/riot:arm32v7-2021.1 .

And then push (to GitLab, assumign you've loged-in): -

    $ docker push registry.gitlab.com/a.b.christie/riot:arm32v7-2021.1

---

[aws iot sdk]: https://docs.aws.amazon.com/iot/latest/developerguide/iot-sdks.html
